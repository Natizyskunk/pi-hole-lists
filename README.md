# pihole-lists
This repo contain all my pi-hole lists !

## Blocklists
- Personal_mega_blocklist : https://gitlab.com/Natizyskunk/pi-hole-lists/raw/master/blocklists/personal_mega_blocklist/hosts.txt

## Whitelists
- Personal_mega_whitelist : https://gitlab.com/Natizyskunk/pi-hole-lists/raw/master/whitelist/personal_mega_whitelist/whitelist.txt

## Blacklists
- Personal_mega_blacklist : https://gitlab.com/Natizyskunk/pi-hole-lists/raw/master/blacklist/personal_mega_blacklist/blacklist.txt
